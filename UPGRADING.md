# Upgrading guide

## From 0.x to 1.x
* The error/prompt/msg fields have been removed from the base twig files, any error is sent through flash messages (with key 'notice'), so you should handle them accordingly.
* password_reset.html.twig has been renamed to password_reset_request.html.twig 
* New TWIG: password_reset_request_end.html.twig. this page is called after a successful password reset request has been sent. This automatically redirects to the login route after 3 secs. If you need to customize this page, you should override it accordingly.