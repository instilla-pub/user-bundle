<?php
/**
 * Created by PhpStorm.
 * User: luca
 * Date: 20/02/19
 * Time: 11.49
 */

namespace Instilla\Bundle\UserBundle\DependencyInjection;


use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

// https://symfony.com/doc/current/bundles/configuration.html

class Configuration implements ConfigurationInterface
{

    /**
     * Generates the configuration tree builder.
     *
     * @return \Symfony\Component\Config\Definition\Builder\TreeBuilder The tree builder
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder('instilla_user');

        if (method_exists($treeBuilder, 'getRootNode')) {
            $rootNode = $treeBuilder->getRootNode();
        } else {
            $rootNode = $treeBuilder->root('fos_user');
        }

        $rootNode->children()
            ->scalarNode('user_class')
            ->defaultValue("App\Entity\AppUser")->end();

        $rootNode->children()
            ->scalarNode('agency_class')
            ->defaultValue("App\Entity\AppAgency")->end();

        return $treeBuilder;
    }
}