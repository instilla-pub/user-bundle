<?php
/**
 * Created by PhpStorm.
 * User: luca
 * Date: 07/03/19
 * Time: 12.03
 */

namespace Instilla\Bundle\UserBundle\Security\Exception;


use Symfony\Component\Security\Core\Exception\AccountStatusException;

class AccountDisabledException extends AccountStatusException
{

    public function getMessageKey()
    {
        return 'User is disabled;';
    }
}