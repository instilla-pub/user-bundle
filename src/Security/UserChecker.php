<?php
/**
 * Created by PhpStorm.
 * User: luca
 * Date: 07/03/19
 * Time: 11.57
 */

namespace Instilla\Bundle\UserBundle\Security;
use Instilla\Bundle\UserBundle\Model\BaseUser;
use Instilla\Bundle\UserBundle\Security\Exception\AccountDisabledException;
use Instilla\Bundle\UserBundle\Security\Exception\AccountNotValidatedException;
use Symfony\Component\Security\Core\User\UserChecker as BaseUserChecker;
use Symfony\Component\Security\Core\User\UserInterface as BaseUserInterface;

class UserChecker extends BaseUserChecker
{
    public function checkPreAuth(BaseUserInterface $user)
    {
        if ($user instanceof BaseUser){
            if(!$user->isEmailValidated()){
                $ex = new AccountNotValidatedException();
                $ex->setUser($user);
                throw $ex;
            }
            if(!$user->isActive()){
                $ex = new AccountDisabledException();
                $ex->setUser($user);
                throw $ex;
            }
        }
    }

    public function checkPostAuth(BaseUserInterface $user)
    {
        return;
    }
}