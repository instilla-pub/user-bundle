<?php
/**
 * Created by PhpStorm.
 * User: luca
 * Date: 20/02/19
 * Time: 14.03
 */

namespace Instilla\Bundle\UserBundle\Model;


use Doctrine\Common\Persistence\ObjectManager;
use Instilla\Bundle\UserBundle\Entity\EmailValidationToken;
use Symfony\Component\Security\Core\Encoder\BCryptPasswordEncoder;
use Symfony\Component\Security\Core\Encoder\EncoderFactoryInterface;
use Symfony\Component\Security\Core\Encoder\PasswordEncoderInterface;
use Symfony\Component\Security\Core\User\UserInterface;

class UserManager
{
    private $userClass;
    private $objectManager;
    private $encoderFactory;
    private $mailer;

    public function __construct($userClass, ObjectManager $om, EncoderFactoryInterface $pswEnc, \Swift_Mailer $mailer)
    {
        $this->userClass      = $userClass;
        $this->objectManager  = $om;
        $this->encoderFactory = $pswEnc;
        $this->mailer         = $mailer;
    }

    public function createUser()
    {
        return new $this->userClass();
    }

    public function persistUser(BaseUser $user)
    {
        $this->objectManager->persist($user);
        $this->objectManager->flush();
    }

    public function findUserByEmail($email)
    {
        $repo = $this->objectManager->getRepository($this->userClass);
        return $repo->findOneBy(['email' => $email]);
    }

    public function findUserById($id)
    {
        $repo = $this->objectManager->getRepository($this->userClass);
        return $repo->findOneBy(['id' => $id]);
    }

    public function getClass()
    {
        return $this->userClass;
    }

    public function hashPassword(UserInterface $user, $plainPassword)
    {
        $encoder = $this->encoderFactory->getEncoder($user);
        if ($encoder instanceof BCryptPasswordEncoder) {
        } else {
            throw new \RuntimeException("Only bcrypt password encoder is supported at the moment");
//            $salt = rtrim(str_replace('+', '.', base64_encode(random_bytes(32))), '=');
//            $user->setSalt($salt);
        }
        $hashedPassword = $encoder->encodePassword($plainPassword, $user->getSalt());
        return $hashedPassword;
    }

    public function sendEmail(BaseUser $user, string $subject, string $body)
    {
        // TODO Use config parameter instead of env directly
        $from = getenv("INSTILLA_USER_REGISTRATION_MAILER_FROM");
        if ($from === false){
            throw new \RuntimeException("INSTILLA_USER_REGISTRATION_MAILER_FROM not specified");
        }
        $message = (new \Swift_Message($subject))
            ->setFrom($from)
            ->setTo($user->getUsername())
            ->setBody($body,"text/html");
        $res = $this->mailer->send($message);
        if ($res <= 0){
            throw new \RuntimeException("Error sending email");
        }
        return true;
    }

}