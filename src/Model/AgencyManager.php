<?php
/**
 * Created by PhpStorm.
 * User: luca
 * Date: 20/02/19
 * Time: 14.03
 */

namespace Instilla\Bundle\UserBundle\Model;


use Doctrine\Common\Persistence\ObjectManager;

class AgencyManager
{
    private $agencyClass;
    private $objectManager;
    private $agencyRepo;

    public function __construct($agencyClass, ObjectManager $om)
    {
        $this->agencyClass   = $agencyClass;
        $this->objectManager = $om;
        $this->agencyRepo    = $this->objectManager->getRepository($this->agencyClass);
    }

    public function createAgency()
    {
        return new $this->agencyClass();
    }

    public function persistAgency(BaseAgency $agency)
    {
        $this->objectManager->persist($agency);
        $this->objectManager->flush();
    }

    public function mergeAgency(BaseAgency $agency)
    {
        $this->objectManager->merge($agency);
        $this->objectManager->flush();
    }

    public function findAgencyByName($name)
    {
        return $this->agencyRepo->findOneBy(['name' => $name]);
    }

    public function findAgencyById($id)
    {
        return $this->agencyRepo->findOneBy(['id' => $id]);
    }

    public function getAgencies()
    {
        return $this->agencyRepo->findAll();
    }

    public function getClass()
    {
        return $this->agencyClass;
    }

}