<?php
/**
 * Created by PhpStorm.
 * User: luca
 * Date: 20/02/19
 * Time: 0.09
 */

namespace Instilla\Bundle\UserBundle\Model;


use Doctrine\ORM\Mapping as ORM;

/**
 * In order to correctly link the User Entity with the Agency, you should create 2 entities that extend BaseUserWithAgency and BaseAgency respectively, then
 * override the $agency property in the User to create the relation.
 */


/**
 * @ORM\MappedSuperclass
 */
class BaseUserWithAgency extends BaseUser
{

    /**
     * @var AgencyInterface | null
     */
    protected $agency;

    /**
     * @return BaseAgency
     */
    public function getAgency(): ?AgencyInterface
    {
        return $this->agency;
    }

    /**
     * @param BaseAgency $agency
     * @return BaseUserWithAgency
     */
    public function setAgency(AgencyInterface $agency): BaseUserWithAgency
    {
        $this->agency = $agency;
        return $this;
    }
}