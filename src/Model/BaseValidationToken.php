<?php
/**
 * Created by PhpStorm.
 * User: luca
 * Date: 20/02/19
 * Time: 16.47
 */

namespace Instilla\Bundle\UserBundle\Model;


use Doctrine\ORM\Mapping as ORM;
use Instilla\Bundle\UserBundle\Util\TokenGenerator;

/**
 * @ORM\MappedSuperclass
 * @ORM\HasLifecycleCallbacks
 */
class BaseValidationToken
{
    /**
     * @ORM\Id()
     * @ORM\Column(type="text")
     * @var string
     */
    protected $id;

    /**
     * @ORM\Column(type="guid")
     * @var string
     */
    protected $userId;

    /**
     * @ORM\Column(type="datetime")
     * @var \DateTimeInterface
     */
    protected $expiresAt;
    /**
     * @ORM\Column(type="datetime")
     * @var \DateTimeInterface
     */
    protected $createdAt;

    public function __construct($userId = null, string $interval_spec = "PT1H")
    {
        $this->id        = TokenGenerator::generateToken();
        $this->createdAt = new \DateTime('now');
        $this->expiresAt = clone $this->createdAt;
        $this->userId    = $userId;
        $this->expiresAt->add(new \DateInterval($interval_spec));
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @param string $id
     * @return BaseValidationToken
     */
    public function setId(string $id): BaseValidationToken
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getUserId(): string
    {
        return $this->userId;
    }

    /**
     * @param string $userId
     * @return BaseValidationToken
     */
    public function setUserId(string $userId): BaseValidationToken
    {
        $this->userId = $userId;
        return $this;
    }

    /**
     * @ORM\PrePersist()
     */
    public function prePersist()
    {
        $this->createdAt = new \DateTime('now');
    }

    public function isExpired(): bool
    {
        $now = new \DateTime('now');
        return $now > $this->expiresAt;
    }
}