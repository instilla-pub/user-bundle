<?php
/**
 * Created by PhpStorm.
 * User: luca
 * Date: 20/02/19
 * Time: 0.09
 */

namespace Instilla\Bundle\UserBundle\Model;


use Doctrine\ORM\Mapping as ORM;
use Ramsey\Uuid\Uuid;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @ORM\MappedSuperclass
 * @ORM\HasLifecycleCallbacks
 */
class BaseUser implements UserInterface
{

    /**
     * @ORM\Id()
     * @ORM\Column(type="guid")
     * @var string
     */
    protected $id;
    /**
     * @ORM\Column(type="datetime")
     * @var \DateTimeInterface
     */
    protected $updatedAt;
    /**
     * @ORM\Column(type="datetime")
     * @var \DateTimeInterface
     */
    protected $createdAt;

    /**
     * @ORM\Column(type="string", unique=true, length=255)
     * @var string
     */
    protected $email;
    /**
     * @ORM\Column(type="string", length=255)
     * @var string
     */
    protected $password;
    /**
     * @ORM\Column(type="boolean", options={"default": false})
     * @var boolean
     */
    protected $emailValidated;
    /**
     * @ORM\Column(type="json")
     * @var string[]
     */
    protected $roles;
    /**
     * @ORM\Column(type="boolean", options={"default": true})
     * @var bool
     */
    protected $active;

    public function __construct()
    {
        $this->id             = Uuid::uuid4()->toString();
        $this->roles          = [];
        $this->emailValidated = false;
        $this->active         = true;
    }

    /**
     * @return string
     */
    public function getEmail(): ?string
    {
        return $this->email;
    }

    /**
     * @param string $email
     * @return BaseUser
     */
    public function setEmail(string $email): BaseUser
    {
        $this->email = $email;
        return $this;
    }

    /**
     * @return bool
     */
    public function isActive(): bool
    {
        return $this->active;
    }

    /**
     * @param bool $active
     * @return BaseUser
     */
    public function setActive(bool $active): BaseUser
    {
        $this->active = $active;
        return $this;
    }

    public function getId(): ?string
    {
        return $this->id;
    }

    /**
     * @ORM\PrePersist()
     */
    public function prePersist()
    {
        $this->createdAt = new \DateTime('now');
        $this->updatedAt = new \DateTime('now');
    }

    /**
     * @ORM\PreUpdate()
     */
    public function preUpdate()
    {
        $this->updatedAt = new \DateTime('now');
    }

    /**
     * Returns the roles granted to the user.
     *
     *     public function getRoles()
     *     {
     *         return ['ROLE_USER'];
     *     }
     *
     * Alternatively, the roles might be stored on a ``roles`` property,
     * and populated in any number of different ways when the user object
     * is created.
     *
     * @return (Role|string)[] The user roles
     */
    public function getRoles()
    {
        return $this->roles;
    }

    /**
     * Returns the password used to authenticate the user.
     *
     * This should be the encoded password. On authentication, a plain-text
     * password will be salted, encoded, and then compared to this value.
     *
     * @return string The password
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @param string $password
     * @return BaseUser
     */
    public function setPassword(string $password): BaseUser
    {
        $this->password = $password;
        return $this;
    }

    /**
     * Returns the salt that was originally used to encode the password.
     *
     * This can return null if the password was not encoded using a salt.
     *
     * @return string|null The salt
     */
    public function getSalt()
    {
        return null;
    }

    /**
     * Returns the username used to authenticate the user.
     *
     * @return string The username
     */
    public function getUsername()
    {
        return $this->email;
    }

    /**
     * Removes sensitive data from the user.
     *
     * This is important if, at any given point, sensitive information like
     * the plain-text password is stored on this object.
     */
    public function eraseCredentials()
    {
        return;
    }

    public function addRole(string $role): BaseUser
    {
        if (!in_array($role, $this->roles)) {
            $this->roles[] = $role;
        }
        return $this;
    }

    public function removeRole(string $role): BaseUser
    {
        $key = array_search($role, $this->roles);
        if ($key) {
            unset($this->roles[$key]);
        }
        return $this;
    }

    /**
     * @return bool
     */
    public function isEmailValidated(): bool
    {
        return $this->emailValidated;
    }

    /**
     * @param bool $emailValidated
     */
    public function setEmailValidated(bool $emailValidated): BaseUser
    {
        $this->emailValidated = $emailValidated;
        return $this;
    }

    /**
     * @return \DateTimeInterface
     */
    public function getUpdatedAt(): \DateTimeInterface
    {
        return $this->updatedAt;
    }

    /**
     * @return \DateTimeInterface
     */
    public function getCreatedAt(): \DateTimeInterface
    {
        return $this->createdAt;
    }


}