<?php
/**
 * Created by PhpStorm.
 * User: luca
 * Date: 20/02/19
 * Time: 23.53
 */

namespace Instilla\Bundle\UserBundle\Model;


interface AgencyInterface
{
    public function getName(): string;

    public function setName(string $name): AgencyInterface;

    public function getId();

}