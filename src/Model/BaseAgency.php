<?php
/**
 * Created by PhpStorm.
 * User: luca
 * Date: 20/02/19
 * Time: 23.51
 */

namespace Instilla\Bundle\UserBundle\Model;

use Doctrine\ORM\Mapping as ORM;
use Ramsey\Uuid\Uuid;

/**
 * @ORM\MappedSuperclass
 * @ORM\HasLifecycleCallbacks
 */
class BaseAgency implements AgencyInterface
{
    /**
     * @ORM\Id()
     * @ORM\Column(type="guid")
     * @var string
     */
    protected $id;
    /**
     * @ORM\Column(type="datetime")
     * @var \DateTimeInterface
     */
    protected $updatedAt;
    /**
     * @ORM\Column(type="datetime")
     * @var \DateTimeInterface
     */
    protected $createdAt;

    /**
     * @ORM\Column(type="text")
     * @var string
     */
    protected $name;

    public function __construct()
    {
        $this->id = Uuid::uuid4()->toString();
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return BaseAgency
     */
    public function setName(string $name): AgencyInterface
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @ORM\PrePersist()
     */
    public function prePersist()
    {
        $this->createdAt = new \DateTime('now');
        $this->updatedAt = new \DateTime('now');
    }

    /**
     * @ORM\PreUpdate()
     */
    public function preUpdate()
    {
        $this->updatedAt = new \DateTime('now');
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

}