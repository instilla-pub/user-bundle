<?php
/**
 * Created by PhpStorm.
 * User: luca
 * Date: 21/02/19
 * Time: 0.15
 */

namespace Instilla\Bundle\UserBundle\Command;


use Instilla\Bundle\UserBundle\Model\BaseUser;
use Instilla\Bundle\UserBundle\Model\UserManager;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class CreateUserCommand extends Command
{
    private $userManager;


    public function __construct(UserManager $um)
    {
        parent::__construct();
        $this->userManager = $um;
    }

    protected function configure()
    {
        $this->setName("instilla:user:create");
        $this->setDescription("Create a user")
            ->setDefinition([
                new InputArgument('username', InputArgument::REQUIRED, 'Username'),
                new InputArgument('password', InputArgument::REQUIRED, 'Password'),
                new InputOption('super-admin', null, InputOption::VALUE_NONE, 'Set the user as super admin')
            ]);
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $username   = $input->getArgument('username');
        $password   = $input->getArgument('password');
        $superadmin = $input->getOption('super-admin');

        /** @var BaseUser $user */
        $user = $this->userManager->createUser();

        $user->setEmailValidated(true)
            ->setPassword($this->userManager->hashPassword($user, $password))
            ->setEmail($username);

        if ($superadmin) {
            $user->addRole("ROLE_SUPER_ADMIN");
        }

        $this->userManager->persistUser($user);


    }

}