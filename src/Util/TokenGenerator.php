<?php
/**
 * Created by PhpStorm.
 * User: luca
 * Date: 20/02/19
 * Time: 16.32
 */

namespace Instilla\Bundle\UserBundle\Util;


class TokenGenerator
{
    /**
     * {@inheritdoc}
     */
    public static function generateToken()
    {
        return rtrim(strtr(base64_encode(random_bytes(32)), '+/', '-_'), '=');
    }
}