<?php
/**
 * Created by PhpStorm.
 * User: luca
 * Date: 20/02/19
 * Time: 16.42
 */

namespace Instilla\Bundle\UserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Instilla\Bundle\UserBundle\Model\BaseValidationToken;
use Instilla\Bundle\UserBundle\Util\TokenGenerator;

/**
 * @ORM\Entity()
 * @ORM\Table(name="instilla_user_email_validation_token")
 */
class EmailValidationToken extends BaseValidationToken
{

}