<?php

namespace Instilla\Bundle\UserBundle;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpKernel\Bundle\Bundle;

/*
 * Purpose of this bundle is to handle all common tasks related to User Management
 *
 * Author: Luca Nardelli <luca.nardelli@instilla.it>
 */

class InstillaUserBundle extends Bundle
{
    public function build(ContainerBuilder $container)
    {
        parent::build($container);
    }
}

