<?php
/**
 * Created by PhpStorm.
 * User: luca
 * Date: 20/02/19
 * Time: 8.47
 */

namespace Instilla\Bundle\UserBundle\Controller;


use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\ORM\Query\Expr\Base;
use Instilla\Bundle\UserBundle\Entity\EmailValidationToken;
use Instilla\Bundle\UserBundle\Entity\PasswordResetToken;
use Instilla\Bundle\UserBundle\Model\BaseUser;
use Instilla\Bundle\UserBundle\Model\UserManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Security\Csrf\CsrfTokenManagerInterface;

class RegistrationController extends AbstractController
{
    private $tokenManager;
    private $userManager;
    private $objectManager;

    public function __construct(CsrfTokenManagerInterface $tokenManager, UserManager $um, ObjectManager $om)
    {
        $this->tokenManager  = $tokenManager;
        $this->userManager   = $um;
        $this->objectManager = $om;
    }

    public function signupPage(Request $request)
    {
        $csrfToken = $this->tokenManager
            ? $this->tokenManager->getToken('authenticate')->getValue()
            : null;

        return $this->render("@InstillaUser/Registration/signup.html.twig", [
            'csrf_token' => $csrfToken
        ]);
    }

    public function signupAction(Request $request)
    {
        $username = $request->get("_username");
        $pass     = $request->get("_password");
        $pass2    = $request->get("_password2");

        if (!filter_var($username, FILTER_VALIDATE_EMAIL)) {
            $this->addFlash('notice', 'Please input a valid email');
            goto fail;
        }

        if ($pass !== $pass2) {
            $this->addFlash('notice', 'Passwords do not match');
            goto fail;
        }

        /** @var BaseUser $user */
        $user = $this->userManager->findUserByEmail($username);
        if ($user) {
            if ($user->isEmailValidated()) {
                $this->addFlash('notice', 'Username already taken');
                goto fail;
            } else {
                $this->sendUserValidationEmail($user);
                $this->addFlash('notice', 'User already exists but it is not validated. You will receive another validation email!');
                goto fail;
            }
        }

        /** @var BaseUser $user */
        $user = $this->userManager->createUser();
        $user->setEmail($username);
        $user->setPassword($this->userManager->hashPassword($user, $pass));
        $user->addRole("ROLE_USER");
        $this->userManager->persistUser($user);

        $this->sendUserValidationEmail($user);

        return $this->render('@InstillaUser/Registration/signup_end.html.twig',
            [
                'success' => true,
                'email'   => $user->getEmail()
            ]);

//        Fail handler
        fail:
        $csrfToken = $this->tokenManager
            ? $this->tokenManager->getToken('authenticate')->getValue()
            : null;
        return $this->render("@InstillaUser/Registration/signup.html.twig", [
            'csrf_token' => $csrfToken,
            'last_email' => $username
        ]);
    }

    private function sendUserValidationEmail(BaseUser $user)
    {
        // Delete old tokens
        $tokenRepository = $this->objectManager->getRepository(EmailValidationToken::class);
        $existingTokens  = $tokenRepository->findBy(['userId' => $user->getId()]);
        foreach ($existingTokens as $tok) {
            $this->objectManager->remove($tok);
        }
        $this->objectManager->flush();

        // Generate token and persist
        $tok = new EmailValidationToken($user->getId(), "PT1H");
        $this->objectManager->persist($tok);
        $this->objectManager->flush();

        // Send validation email
        $this->userManager->sendEmail($user,
            'Signup confirmation',
            $this->renderView(
                '@InstillaUser/Emails/registration_email_template.html.twig',
                [
                    'email'            => $user->getUsername(),
                    'verification_url' => $this->generateUrl('instilla_user_registration_verify_email_page',
                        ['token' => $tok->getId()], UrlGeneratorInterface::ABSOLUTE_URL)
                ]
            ));
    }

    public function verifyEmailAction(Request $request)
    {
        $token = $request->get("token");
        if (!$token) {
            return $this->render("@InstillaUser/Registration/email_verified.html.twig", [
                'success' => false
            ]);
        }
        $repo = $this->objectManager->getRepository(EmailValidationToken::class);
        /** @var EmailValidationToken $tok */
        $tok = $repo->findOneBy(['id' => $token]);
        if (!$tok) {
            return $this->render("@InstillaUser/Registration/email_verified.html.twig", [
                'success' => false
            ]);
        }

        if ($tok->isExpired()) {
            $this->addFlash('notice', 'Token expired');
            return $this->render("@InstillaUser/Registration/email_verified.html.twig", [
                'success' => false
            ]);
        }

        /** @var BaseUser $user */
        $user = $this->userManager->findUserById($tok->getUserId());
        if (!$user) {
            return $this->render("@InstillaUser/Registration/email_verified.html.twig", [
                'success' => false
            ]);
        }
        $user->setEmailValidated(true);
        $this->objectManager->remove($tok);
        $this->objectManager->merge($user);
        $this->objectManager->flush();

        return $this->render("@InstillaUser/Registration/email_verified.html.twig", [
            'success' => true
        ]);
    }

    public function resetPasswordPage()
    {
        $csrfToken = $this->tokenManager
            ? $this->tokenManager->getToken('authenticate')->getValue()
            : null;

        return $this->render("@InstillaUser/Registration/password_reset_request.html.twig", [
            'csrf_token' => $csrfToken
        ]);
    }

    public function resetPasswordAction(Request $request)
    {
        $username = $request->get("_username");

        /** @var BaseUser $user */
        $user = $this->userManager->findUserByEmail($username);

        // Non existing user
        if ($user === null) {
            goto end;
        }

        // Delete all previously existing tokens for the user
        $tokenRepository = $this->objectManager->getRepository(PasswordResetToken::class);
        $existingTokens  = $tokenRepository->findBy(['userId' => $user->getId()]);
        foreach ($existingTokens as $tok) {
            $this->objectManager->remove($tok);
        }
        $this->objectManager->flush();

        $tok = new PasswordResetToken($user->getId(), "PT1H");
        $this->objectManager->persist($tok);
        $this->objectManager->flush();

        $this->userManager->sendEmail($user,
            'Password reset',
            $this->renderView(
                '@InstillaUser/Emails/password_reset_email_template.html.twig',
                [
                    'email'            => $user->getUsername(),
                    'verification_url' => $this->generateUrl('instilla_user_registration_do_reset_password_page',
                        ['token' => $tok->getId()], UrlGeneratorInterface::ABSOLUTE_URL)
                ]
            ));

        end:
        return $this->render("@InstillaUser/Registration/password_reset_request_end.html.twig", [
            'success' => true
        ]);
    }

    public function doResetPasswordPage(Request $request)
    {
        $token = $request->get("token");
        if (!$token) {
            $this->addFlash('notice','Missing token');
            return $this->render("@InstillaUser/Registration/do_password_reset_end.html.twig", [
                'success' => false
            ]);
        }

        $repo = $this->objectManager->getRepository(PasswordResetToken::class);
        /** @var EmailValidationToken $tok */
        $tok = $repo->findOneBy(['id' => $token]);
        if (!$tok) {
            $this->addFlash('notice','Token not found');
            return $this->render("@InstillaUser/Registration/do_password_reset_end.html.twig", [
                'success' => false
            ]);
        }

        $csrfToken = $this->tokenManager
            ? $this->tokenManager->getToken('authenticate')->getValue()
            : null;

        return $this->render("@InstillaUser/Registration/do_password_reset.html.twig", [
            'csrf_token' => $csrfToken,
            'token'      => $token
        ]);
    }

    public function doResetPasswordAction(Request $request)
    {
        $pass  = $request->get("_password");
        $pass2 = $request->get("_password2");
        $token = $request->get("_token");
        if ($pass !== $pass2) {
            $this->addFlash('notice', 'Passwords do not match');
            $csrfToken = $this->tokenManager
                ? $this->tokenManager->getToken('authenticate')->getValue()
                : null;
            return $this->render("@InstillaUser/Registration/do_password_reset.html.twig", [
                'csrf_token' => $csrfToken,
                'token'      => $token
            ]);
        }
        if (!$token) {
            $this->addFlash('notice', 'No token sent');
            return $this->render("@InstillaUser/Registration/do_password_reset_end.html.twig", [
                'success' => false
            ]);
        }

        $repo = $this->objectManager->getRepository(PasswordResetToken::class);
        /** @var EmailValidationToken $tok */
        $tok = $repo->findOneBy(['id' => $token]);
        if (!$tok) {
            $this->addFlash('notice', 'No token stored');
            return $this->render("@InstillaUser/Registration/do_password_reset_end.html.twig", [
                'success' => false
            ]);
        }

        if ($tok->isExpired()) {
            $this->addFlash('notice', 'Token expired');
            return $this->render("@InstillaUser/Registration/do_password_reset_end.html.twig", [
                'success' => false
            ]);
        }

        /** @var BaseUser $user */
        $user = $this->userManager->findUserById($tok->getUserId());
        if (!$user) {
            $this->addFlash('notice', 'No user found');
            return $this->render("@InstillaUser/Registration/do_password_reset_end.html.twig", [
                'success' => false
            ]);
        }
        $user->setPassword($this->userManager->hashPassword($user, $pass));
        $this->objectManager->remove($tok);
        $this->objectManager->merge($user);
        $this->objectManager->flush();

        return $this->render("@InstillaUser/Registration/do_password_reset_end.html.twig", [
            'success' => true
        ]);
    }

}
