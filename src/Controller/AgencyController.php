<?php
/**
 * Created by PhpStorm.
 * User: luca
 * Date: 20/02/19
 * Time: 8.47
 */

namespace Instilla\Bundle\UserBundle\Controller;


use Doctrine\Common\Persistence\ObjectManager;
use Instilla\Bundle\UserBundle\Model\AgencyManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Security\Csrf\CsrfTokenManagerInterface;

class AgencyController extends AbstractController
{
    private $tokenManager;
    private $agencyManager;
    private $objectManager;

    public function __construct(CsrfTokenManagerInterface $tokenManager, AgencyManager $am, ObjectManager $om)
    {
        $this->tokenManager = $tokenManager;
        $this->agencyManager = $am;
        $this->objectManager = $om;
    }

    public function index(Request $request)
    {
        return $this->render("@InstillaUser/Agency/index.html.twig",[
            'agencies' => $this->agencyManager->getAgencies()
        ]);
    }

    public function newAgency(Request $request)
    {
        return $this->render("@InstillaUser/Agency/index.html.twig",[
            'agencies' => $this->agencyManager->getAgencies()
        ]);
    }

    public function show(Request $request, $id)
    {
        return $this->render("@InstillaUser/Agency/index.html.twig",[
            'agencies' => $this->agencyManager->getAgencies()
        ]);
    }

    public function edit(Request $request, $id)
    {
        return $this->render("@InstillaUser/Agency/index.html.twig",[
            'agencies' => $this->agencyManager->getAgencies()
        ]);
    }

}
