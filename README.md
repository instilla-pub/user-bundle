# Instilla user bundle

A Symfony bundle with some common User functionality used in Instilla's apps

## Installation

`composer require instilla/user-bundle`